package dao;

import domain.Tweet;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Stateless
public class TweetDao {

    @PersistenceContext
    EntityManager em;

    public List allTweets() { return em.createNamedQuery("tweet.allTweets").getResultList();}

    public Tweet find(Long id) { return em.find(Tweet.class, id);}

    public Tweet save(Tweet t){ em.persist(t); return t;}

    public Tweet remove(Long id){
        Tweet tweet = find(id);
        em.remove(tweet);
        return tweet;
    }

    public Tweet edit(Tweet tweet) {
        em.merge(tweet);
        return tweet;
    }

    public List<Tweet> searchTweets(String search) {
        em.getEntityManagerFactory().getCache().evictAll();
        return em.createNamedQuery("tweet.searchTweets").setParameter("search", search).getResultList();
    }
}
