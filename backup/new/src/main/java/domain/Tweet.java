package domain;

import javax.json.bind.annotation.JsonbTransient;
import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
@NamedQueries({
        @NamedQuery(name = "tweet.allTweets", query = "select t from Tweet t"),
        @NamedQuery(name = "tweet.getTweetsFromUser", query = "select t from Tweet t where t.postedBy = :user_id"),
        @NamedQuery(name = "tweet.searchTweets", query = "select t from Tweet t where t.content LIKE CONCAT('%', :search, '%')")
})
public class Tweet {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Temporal(TemporalType.DATE)
    private Date postDate;

    private String content;

    @ManyToOne
    private User postedBy;

    @ManyToMany(mappedBy = "likedTweets")
    @JsonbTransient
    private List<User> likedBy;

    public Tweet() {
    }

    public Tweet(Date postDate, String content, User postedBy) {
        this.postDate = postDate;
        this.content = content;
        this.postedBy = postedBy;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getPostDate() {
        return postDate;
    }

    public void setPostDate(Date postDate) {
        this.postDate = postDate;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public User getPostedBy() {
        return postedBy;
    }

    public void setPostedBy(User postedBy) {
        this.postedBy = postedBy;
    }

    public List<User> getLikedBy() {
        return likedBy;
    }

    public void setLikedBy(List<User> likedBy) {
        this.likedBy = likedBy;
    }

    @Override
    public String toString() {
        return "Tweet{" +
                "id=" + id +
                ", postDate=" + postDate +
                ", content='" + content + '\'' +
                ", postedBy=" + postedBy +
                ", likedBy=" + likedBy +
                '}';
    }
}
