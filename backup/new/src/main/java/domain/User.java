package domain;

import javax.json.bind.annotation.JsonbTransient;
import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@NamedQueries({
        @NamedQuery(name = "user.allUsers", query = "select u from User u "),
        @NamedQuery(name = "user.getUserByName", query = "select u from User u where u.username = :name"),
        @NamedQuery(name = "user.deleteUserById", query = "delete from User u where u.id = :id")
})
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(unique = true, length = 30)
    private String username;

    @Column (length = 30, nullable = false)
    private String password;

    @Column (unique = true, nullable = false)
    private String email;

    private String bio;
    private String location;
    private String website;

    @Column (nullable = false)
    private boolean isModerator;

    @ManyToMany
    @JsonbTransient
    @JoinTable(name = "follow",
            joinColumns = @JoinColumn(name = "follower_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "following_id", referencedColumnName = "id"))
    private List<User> followers;

    @ManyToMany(mappedBy = "followers")
    @JsonbTransient
    private List<User> following;

    @JoinColumn(name = "posted_tweet")
    @JsonbTransient
    @OneToMany(mappedBy = "postedBy", cascade = CascadeType.PERSIST)
    private List<Tweet> postedTweets;

    @ManyToMany
    @JoinTable(name = "User_Like",
            joinColumns = @JoinColumn(name = "user_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "tweet_id", referencedColumnName = "id"))
    private List<Tweet> likedTweets;

    public User() {
    }

    public User(String username, String password, String email, String bio, String location, String website, boolean isModerator) {
        this.username = username;
        this.password = password;
        this.email = email;
        this.bio = bio;
        this.location = location;
        this.website = website;
        this.isModerator = isModerator;
    }

    public List<User> getFollowers() {
        return followers;
    }

    public void setFollowers(List<User> followers) {
        this.followers = followers;
    }

    public List<User> getFollowing() {
        if(following == null)
            following = new ArrayList<>();

        return following;
    }

    public void setFollowing(List<User> following) {
        this.following = following;
    }

    public List<Tweet> getPostedTweets() {
        return postedTweets;
    }

    public void setPostedTweets(List<Tweet> postedTweets) {
        this.postedTweets = postedTweets;
    }

    public List<Tweet> getLikedTweets() {
        return likedTweets;
    }

    public void setLikedTweets(List<Tweet> likedTweets) {
        this.likedTweets = likedTweets;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getBio() {
        return bio;
    }

    public void setBio(String bio) {
        this.bio = bio;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public boolean isModerator() {
        return isModerator;
    }

    public void setModerator(boolean moderator) {
        isModerator = moderator;
    }

    public void addFollower(User u){
        if(this.getFollowers() != null)
            this.getFollowers().add(u);
        else {
            this.followers = new ArrayList<User>();
            this.followers.add(u);
        }

    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", email='" + email + '\'' +
                ", bio='" + bio + '\'' +
                ", location='" + location + '\'' +
                ", website='" + website + '\'' +
                ", isModerator=" + isModerator +
                ", followers=" + followers +
                ", following=" + following +
                ", postedTweets=" + postedTweets +
                ", likedTweets=" + likedTweets +
                '}';
    }
}
