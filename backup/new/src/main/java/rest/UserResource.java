package rest;

import domain.Role;
import domain.User;
import service.Secured;
import service.UserService;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.json.Json;
import javax.json.JsonObject;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.util.List;

@Path("users")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@Stateless
public class UserResource {

    @Inject
    UserService userService;


    @POST
    @Path("auth")
    public Response authenticate(User user) {
        String token = userService.authenticate(user);
        JsonObject jsonToken = Json.createObjectBuilder().add("token", token).build();
        if (token != null) {
            return Response.ok(jsonToken, MediaType.APPLICATION_JSON).build();
        }
        return Response.status(Response.Status.FORBIDDEN).build();
    }
    @GET
    @Path("user")
    @Secured
    public Response getAuthenticatedUser() {
        User user = userService.getAuthenticatedUser();
        return Response.ok(user).build();
    }

    @GET
    @Secured(Role.MODERATOR)
    public List<User> allUsers(){
        return userService.allUsers();
    }

    @GET
    @Secured
    @Path("{id}")
    public User getUserById(@PathParam("id") long id){ return userService.findUserById(id);}

    @GET
    @Path("username/{name}")
    public User getUserByName(@PathParam("name") String name){
        return userService.findUserByName(name);
    }

    @GET
    @Secured
    @Path("{id}/followers")
    public List<User> getFollowers(@PathParam("id") long id) {return userService.findUserById(id).getFollowers(); }

    @GET
    @Secured
    @Path("{id}/following")
    public List<User> getFollowing(@PathParam("id") long id) {return userService.findUserById(id).getFollowing(); }

    @POST
    public Response addUser(User user, @Context UriInfo uriInfo) {
        User u = userService.addUser(user);
        return Response.ok(u).build();
    }

    @PUT
    @Secured
    public Response editUser(User user, @Context UriInfo uriInfo) {
        User u = userService.editUser(user);
        return Response.ok(u).build();
    }

    @PUT
    @Secured
    @Path("{follower_id}/{following_id}")
    public Response follow(@PathParam("follower_id") Long follower_id, @PathParam("following_id") Long following_id){
        User follower = userService.findUserById(follower_id);
        User following = userService.findUserById(following_id);
        following.addFollower(follower);
        follower.getFollowing().add(following);
        userService.editUser(follower);
        userService.editUser(following);
        return Response.ok().build();
    }

    @PUT
    @Secured
    @Path("/removeFollower/{follower_id}/{following_id}")
    public Response unFollow(@PathParam("follower_id") Long follower_id, @PathParam("following_id") Long following_id){
        User follower = userService.findUserById(follower_id);
        User following = userService.findUserById(following_id);
        follower.getFollowing().remove(following);
        following.getFollowers().remove(follower);
        userService.editUser(follower);
        userService.editUser(following);
        return Response.ok().build();
    }

}
