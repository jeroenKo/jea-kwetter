package service;


import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureException;

import javax.annotation.Priority;
import javax.enterprise.event.Event;
import javax.inject.Inject;
import javax.ws.rs.Priorities;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.Provider;
import java.security.Key;

@Secured
@Provider
@Priority(Priorities.AUTHENTICATION)
public class AuthenticationFilter implements ContainerRequestFilter{
    private static final String AUTHENTICATION_SCHEME = "Bearer";
    private static final String REALM = "example";

    @Inject
    @AuthenticatedUser
    private Event<String> userAuthenticatedEvent;


    public Response getUnauthorizedResponse() {
        return Response.status(Response.Status.UNAUTHORIZED)
                .header(HttpHeaders.WWW_AUTHENTICATE, AUTHENTICATION_SCHEME + " realm=\"" + REALM + "\"")
                .build();
    }

    @Override
    public void filter(ContainerRequestContext containerRequestContext) {
        String authorizationHeader = containerRequestContext.getHeaderString(HttpHeaders.AUTHORIZATION);

        //Validate the authorization header
        if (authorizationHeader == null || authorizationHeader.isEmpty() || !authorizationHeader.toLowerCase().startsWith(AUTHENTICATION_SCHEME.toLowerCase() + " ")) {
            containerRequestContext.abortWith(getUnauthorizedResponse());
            return;
        }

        //extract the token from the authorization header
        String token = authorizationHeader.substring(AUTHENTICATION_SCHEME.length()).trim();

        //validate the token
        Key key = Config.getKey();
        try {
            String userName = Jwts.parser().setSigningKey(key).parseClaimsJws(token).getBody().getSubject();
            userAuthenticatedEvent.fire(userName);
        }
        catch(IllegalArgumentException|SignatureException e) {
            containerRequestContext.abortWith(getUnauthorizedResponse());
        }
    }

    public static String getAuthenticationScheme() {
        return AUTHENTICATION_SCHEME;
    }

    public static String getREALM() {
        return REALM;
    }

    public Event<String> getUserAuthenticatedEvent() {
        return userAuthenticatedEvent;
    }

    public void setUserAuthenticatedEvent(Event<String> userAuthenticatedEvent) {
        this.userAuthenticatedEvent = userAuthenticatedEvent;
    }
}
