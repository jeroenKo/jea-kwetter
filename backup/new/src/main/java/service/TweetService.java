package service;

import dao.TweetDao;
import domain.Tweet;

import javax.ejb.Stateless;
import javax.inject.Inject;
import java.util.List;

@Stateless
public class TweetService {

    @Inject
    TweetDao tweetDAO;

    public List<Tweet> allTweets() {
        return tweetDAO.allTweets();
    }

    public Tweet findTweet(long id) {
        return tweetDAO.find(id);
    }

    public Tweet removeTweet(Long id){
        return tweetDAO.remove(id);
    }

    public Tweet addTweet(Tweet tweet) {
        return tweetDAO.save(tweet);
    }

    public Tweet editTweet(Tweet tweet) {
        return tweetDAO.edit(tweet);
    }

    public List<Tweet> searchTweets(String searchTerm) {
        return tweetDAO.searchTweets(searchTerm);
    }
}
