package util;

import dao.TweetDao;
import dao.UserDao;
import domain.Tweet;
import domain.User;

import javax.annotation.PostConstruct;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.inject.Inject;
import java.util.Date;

@Startup
@Singleton
public class init {
    @Inject
    UserDao userService;

    @Inject
    TweetDao tweetDAO;

    @PostConstruct
    public void init() {
        System.out.println("init begin ......................................................");
        User u = new User("jeroen", "passwrd", "e@mail.com", "bio", "Eindje", "web.site", true);
        Tweet t = new Tweet(new Date(2018 - 1900, 9, 24), "#content", u);

        User u1 = new User("tim", "passwrd", "example@email.com", "Lonewolf", "Vliss", "web.site", false);
        Tweet t1 = new Tweet(new Date(2018 - 1900, 15, 24), "WHAT IS UP THE WOOORLD", u1);

        u.addFollower(u1);
        userService.save(u);
        tweetDAO.save(t);
        userService.save(u1);
        tweetDAO.save(t1);
        u.getPostedTweets().add(t);
        u1.getPostedTweets().add(t1);
    }
}
