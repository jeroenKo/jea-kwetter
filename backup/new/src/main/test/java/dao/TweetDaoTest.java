package dao;

import domain.Tweet;
import domain.User;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import util.DatabaseCleaner;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;

import static java.util.logging.Logger.getLogger;

public class TweetDaoTest {
    private EntityManagerFactory emf = Persistence.createEntityManagerFactory("integrationTestPU");
    private EntityManager em;
    private EntityTransaction tx;

    private TweetDao tweetDao = new TweetDao();
    private UserDao userDao = new UserDao();

    User u = new User("jeroen", "passwrd", "e@mail.com", "bio", "Eindje", "web.site", true);
    User u1 = new User("tim", "passwrd", "example@email.com", "Lonewolf", "Vliss", "web.site", false);
    User u2 = new User("ties", "passwrd", "ex@mple.com", "Jonguh", "st Joost", "web.site", false);

    Tweet t = new Tweet(new Date(2018 - 1900, 9, 24), "#content", u);
    Tweet t1 = new Tweet(new Date(2018 - 1900, 3, 2), "WHAT IS UP THE WOOORLD", u1);
    Tweet t2 = new Tweet(new Date(2017 - 1900, 1, 20), "Hey there, i am using Kwetter", u2);

    @Before
    public void setUp() {
        try {
            new DatabaseCleaner(emf.createEntityManager()).clean();
        } catch (SQLException ex) {
            getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
        }

        em = emf.createEntityManager();
        tx = em.getTransaction();
        tweetDao.em = em;
        userDao.em = em;

        em.getTransaction().begin();
        userDao.save(u);
        userDao.save(u1);
        userDao.save(u2);
        tweetDao.save(t);
        tweetDao.save(t1);
        em.getTransaction().commit();
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testGetAllTweets(){
        Assert.assertEquals(2, tweetDao.allTweets().size());
    }

    @Test
    public void testFindTweet(){
        Tweet result = tweetDao.find(t.getId());
        Assert.assertEquals(result, t);
    }

    @Test
    public void testEditTweet(){
        Tweet tweet = new Tweet(new Date(), "new content", u1);
        tweet.setId(t1.getId());
        Assert.assertNotEquals(tweet, tweetDao.searchTweets("WHAT"));
        em.getTransaction().begin();
        tweetDao.edit(tweet);
        em.getTransaction().commit();
        Assert.assertEquals(tweet.getId(),t1.getId() );
        Assert.assertEquals(tweet.getPostedBy(),t1.getPostedBy() );
        Assert.assertEquals(tweet.getContent(),t1.getContent() );
    }

    @Test
    public void testRemoveTweet(){
        Assert.assertEquals(2, tweetDao.allTweets().size());
        em.getTransaction().begin();
        tweetDao.save(t2);
        em.getTransaction().commit();
        Assert.assertEquals(3, tweetDao.allTweets().size());
        em.getTransaction().begin();
        tweetDao.remove(t2.getId());
        em.getTransaction().commit();
        Assert.assertEquals(2, tweetDao.allTweets().size());
    }

    @Test
    public void testSaveTweet(){
        Assert.assertEquals(2, tweetDao.allTweets().size());
        em.getTransaction().begin();
        tweetDao.save(t2);
        em.getTransaction().commit();
        Assert.assertEquals(3, tweetDao.allTweets().size());
    }

    @Test
    public void testSearchTweet(){
        List<Tweet> results = tweetDao.searchTweets("#content");
        Assert.assertEquals(1, results.size());
        Assert.assertEquals(u, results.get(0).getPostedBy());
    }
}