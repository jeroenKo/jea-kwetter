package dao;

import domain.Tweet;
import domain.User;
import org.junit.*;
import util.DatabaseCleaner;

import javax.naming.NamingException;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import java.sql.SQLException;
import java.util.logging.Level;

import static java.util.logging.Logger.getLogger;



public class UserDaoTest {

    private EntityManagerFactory emf = Persistence.createEntityManagerFactory("integrationTestPU");
    private EntityManager em;
    private EntityTransaction tx;


    UserDao userDao = new UserDao();

    User u = new User("jeroen", "passwrd", "e@mail.com", "bio", "Eindje", "web.site", true);
    User u1 = new User("tim", "passwrd", "example@email.com", "Lonewolf", "Vliss", "web.site", false);
    User u2 = new User("ties", "passwrd", "ex@mple.com", "Jonguh", "st Joost", "web.site", false);
    User u3 = new User("thomas", "passwrd", "email@email.com", "jup", "ergens in limburg", "web.site", false);

    @Before
    public void setUp() {
        try {
            new DatabaseCleaner(emf.createEntityManager()).clean();
        } catch (SQLException ex) {
            getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
        }

        em = emf.createEntityManager();
        tx = em.getTransaction();
        userDao.em = em;

        em.getTransaction().begin();
        userDao.save(u);
        userDao.save(u1);
        userDao.save(u2);
        em.getTransaction().commit();
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testGetAllUsers(){
        Assert.assertEquals(3, userDao.allUsers().size());
    }

    @Test
    public void testSaveUser(){
        Assert.assertEquals(3, userDao.allUsers().size());
        em.getTransaction().begin();
        userDao.save(u3);
        em.getTransaction().commit();
        Assert.assertEquals(4, userDao.allUsers().size());
    }

    @Test
    public void testGetUser(){
        User user = userDao.findUserByName("jeroen");
        Assert.assertEquals(user, u);
        Assert.assertEquals(u, userDao.findUserById(user.getId()));
    }

    @Test
    public void testEditUser(){
        User newU = new User();
        newU.setUsername("jeroen");
        newU.setBio("new bio");
        newU.setPassword("newPassword");
        newU.setEmail("new@mail.com");
        newU.setId(u.getId());
        em.getTransaction().begin();
        userDao.edit(newU);
        em.getTransaction().commit();
        User newUser = userDao.findUserByName("jeroen");
        Assert.assertEquals("new bio", newUser.getBio());
        Assert.assertEquals("newPassword", newUser.getPassword());
        Assert.assertEquals("new@mail.com", newUser.getEmail());
        u.setUsername("jeroenk");
        Assert.assertEquals("jeroenk", userDao.findUserById(newUser.getId()).getUsername());
    }
}
