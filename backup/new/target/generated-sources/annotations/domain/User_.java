package domain;

import domain.Tweet;
import domain.User;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2018-03-10T17:01:23")
@StaticMetamodel(User.class)
public class User_ { 

    public static volatile ListAttribute<User, Tweet> likedTweets;
    public static volatile SingularAttribute<User, String> password;
    public static volatile SingularAttribute<User, String> website;
    public static volatile ListAttribute<User, User> followers;
    public static volatile SingularAttribute<User, Boolean> isModerator;
    public static volatile ListAttribute<User, User> following;
    public static volatile SingularAttribute<User, String> bio;
    public static volatile SingularAttribute<User, String> location;
    public static volatile ListAttribute<User, Tweet> postedTweets;
    public static volatile SingularAttribute<User, Long> id;
    public static volatile SingularAttribute<User, String> email;
    public static volatile SingularAttribute<User, String> username;

}