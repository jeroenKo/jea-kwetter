package dao;

import domain.Tweet;
import domain.User;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Stateless
public class TweetDAO {

    @PersistenceContext
    private EntityManager em;

    public List allTweets() { return em.createNamedQuery("tweet.allTweets").getResultList();}

    public Tweet find(Long id) { return em.find(Tweet.class, id);}

    public Tweet save(Tweet t){ em.persist(t); return t;}

    public Tweet remove(Long id){
        Tweet tweet = find(id);
        em.remove(tweet);
        return tweet;
    }

    public Tweet edit(Tweet tweet) {
        em.merge(tweet);
        return tweet;
    }
}
