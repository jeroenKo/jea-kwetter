package dao;

import domain.User;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

@Stateless
public class UserDAO {

    @PersistenceContext
    private EntityManager em;


    public List allUsers() {
        return em.createNamedQuery("user.allUsers").getResultList();
    }

    public User findUserById(Long id) {
        return em.find(User.class, id);
    }

    public User save(User u){ em.persist(u); return u; }

    public User findUserByName(String name) {
        return (User) em.createNamedQuery("user.getUserByName").setParameter("name",name).getSingleResult();
    }

    public User edit(User user){
        em.merge(user);
        return user;
    }

    public boolean findToken(String token) {
        List results = em.createNamedQuery("user.getToken")
                .setParameter("token", token)
                .getResultList();
        if(results != null && results.size() > 0)
            return true;
        else
            return false;
    }
}
