package domain;

public enum Role {
    USER,
    MODERATOR
}
