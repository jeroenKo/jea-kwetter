package rest;

import domain.Tweet;
import domain.User;
import service.TweetService;
import service.UserService;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Path("tweets")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@Stateless
public class TweetResource {

    @Inject
    TweetService tweetService;

    @Inject
    UserService userService;

    @GET
    public List<Tweet> allTweets() {
        return tweetService.allTweets();
    }

    @GET
    @Path("{id}")
    public Tweet getTweet(@PathParam("id") long id) {
        return tweetService.findTweet(id);
    }

    @GET
    @Path("{id}/tweets")
    public List<Tweet> getTweetsFromUser(@PathParam("id") Long id) {
        User u = userService.findUserById(id);
        return u.getPostedTweets();
    }

    @DELETE
    @Path("/{id}")
    public Response removeTweet(@PathParam("id") Long id, @Context UriInfo uriInfo) {
        Tweet t = tweetService.removeTweet(id);
        return Response.ok(t).build();
    }

    @POST
    public Tweet addTweet(Tweet tweet, @Context UriInfo uriInfo) {
        return tweetService.addTweet(tweet);
    }

    @PUT
    public Tweet editTweet(Tweet tweet, @Context UriInfo uriInfo) {
        return tweetService.editTweet(tweet);
    }

    @PUT
    @Path("{user_id}/{tweet_id}")
    public Response likeTweet(@PathParam("user_id") Long user_id, @PathParam("tweet_id") Long id) {
        User user = userService.findUserById(user_id);
        Tweet tweet = tweetService.findTweet(id);
        user.getLikedTweets().add(tweet);
        tweet.getLikedBy().add(user);
        userService.editUser(user);
        tweetService.editTweet(tweet);
        return Response.ok().build();
    }

    @PUT
    @Path("removeLike/{user_id}/{tweet_id}")
    public Response unlikeTweet(@PathParam("user_id") Long user_id, @PathParam("tweet_id") Long id) {
        User user = userService.findUserById(user_id);
        Tweet tweet = tweetService.findTweet(id);
        user.getLikedTweets().remove(tweet);
        tweet.getLikedBy().remove(user);
        userService.editUser(user);
        tweetService.editTweet(tweet);
        return Response.ok().build();
    }

    @GET
    @Path("getTimeline/{id}")
    public Collection<Tweet> getTimelineForUser(@PathParam("id") Long user_id) {
        List<User> follows = userService.findUserById(user_id).getFollowers();
        ArrayList<Tweet> allTweets = new ArrayList<>();
        for (User u : follows) {
            allTweets.addAll(getTweetsFromUser(u.getId()));
        }
        return allTweets;
    }

    @GET
    @Path("{id}/postedTweets")
    public Collection<Tweet> getPostedTweets(@PathParam("id") long id){ return userService.findUserById(id).getPostedTweets(); }

    @GET
    @Path("{id}/likedTweets")
    public Collection<Tweet> getLikedTweets(@PathParam("id") long id){ return userService.findUserById(id).getLikedTweets(); }
}