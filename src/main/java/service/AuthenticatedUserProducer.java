package service;

import dao.UserDAO;
import domain.User;

import javax.enterprise.context.RequestScoped;
import javax.enterprise.event.Observes;
import javax.enterprise.inject.Produces;
import javax.inject.Inject;

@RequestScoped
public class AuthenticatedUserProducer {
    @Inject
    private UserDAO userDAO;

    @Produces
    @RequestScoped
    @AuthenticatedUser
    private User authenticatedUser;

    public void handleAuthenticationEvent(@Observes @AuthenticatedUser String userName) {
        authenticatedUser = userDAO.findUserByName(userName);
    }
}
