package service;

import io.jsonwebtoken.impl.crypto.MacProvider;

import javax.ejb.Singleton;
import javax.ejb.Startup;
import java.security.Key;

@Startup
@Singleton
public class Config {
    private static Key key;

    public static Key getKey() {
        if (key == null) {
            key = MacProvider.generateKey();
        }
        return key;
    }
}
