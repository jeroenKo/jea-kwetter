package service;

import dao.TweetDAO;
import domain.Tweet;
import domain.User;

import javax.ejb.Stateless;
import javax.inject.Inject;
import java.util.Collection;
import java.util.List;

@Stateless
public class TweetService {

    @Inject
    TweetDAO tweetDAO;

    public List<Tweet> allTweets() {
        return tweetDAO.allTweets();
    }

    public Tweet findTweet(long id) {
        return tweetDAO.find(id);
    }

    public Tweet removeTweet(Long id){
        return tweetDAO.remove(id);
    }

    public Tweet addTweet(Tweet tweet) {
        return tweetDAO.save(tweet);
    }

    public Tweet editTweet(Tweet tweet) {
        return tweetDAO.edit(tweet);
    }
}
