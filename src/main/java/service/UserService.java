package service;

import dao.UserDAO;
import domain.User;
import io.jsonwebtoken.CompressionCodecs;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import util.Hash;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.security.Key;
import java.util.Date;
import java.util.List;

@Stateless
@Produces(MediaType.APPLICATION_JSON)
public class UserService {

    @Inject
    UserDAO userDAO;

    @Inject
    @AuthenticatedUser
    private User authUser;

    public List<User> allUsers() {
        return userDAO.allUsers();
    }

    public User findUserById(long id) {
        return userDAO.findUserById(id);
    }

    public User findUserByName(String name) {
        return userDAO.findUserByName(name);
    }

    public User addUser(User user) {
        return userDAO.save(user);
    }

    public User editUser(User user) {
        return userDAO.edit(user);
    }

    public String authenticate(User user) {
        //user.setPassword(Hash.stringToHash(user.getPassword()));
        User storedUser = userDAO.findUserByName(user.getUsername());
        if (storedUser != null && user.getPassword().equals(storedUser.getPassword())) {
            return generateToken(user);
        }
        return null;
    }
    public User getAuthenticatedUser() {
        return userDAO.findUserById(authUser.getId());
    }

    public String generateToken(User user) {
        Date now = new Date(System.currentTimeMillis());

        Key key = Config.getKey();
        return Jwts.builder()
                .setIssuedAt(now)
                .setSubject(user.getUsername())
                .compressWith(CompressionCodecs.DEFLATE)
                .signWith(SignatureAlgorithm.HS512, key)
                .compact();
    }


}